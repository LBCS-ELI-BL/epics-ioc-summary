# LBCS-ELI-BL
(Laser Beamlines Control System of ELI-BeamLines facility, www.eli-beams.eu)

## Summary 
wip here...


0. Bus (API)

1. Modbus (Asyn)
  * "MOXA" ioLogik E1260 6xRTD
  * "MOXA" ioLogik E1214 6x RelayDO, 6x DI
  * "MOXA" ioLogik E1240 8x AI
  * "MOXA" ioLogik E1241 4x AO
  * "MOXA" ioLogik E1242 4x AI, 4x DI, 4x DIO
  * "PILZ" PSSu PLC, PSSu IO //custom modbus registers

2. RS232 (Streams)
  * "H.I.B (Hydac International)" FWKS- series
  * "H.I.B (Hydac International)" RKH- series //TBD.
  * "Termotek" P307
  * "Termotek" P310 //TBD.

2. RS485 (Streams)
  * "SMC" HEC003-W5A- //TBD.

3. USB (Streams)
  * "Precision Test Systems LTd." GPS10eR //TBD.

4. Ethernet (Streams)
  * "Delta Elektronika BV" DE-SM3300 series SM66-AR-110
  * "H.I.B (Hydac International)" WKW- series

5. Ethernet (SNMP)
  * "Hartmann Electronic (W-IE-NE-R Plein & Baus GmbH)" CML00 management module for cPCI crate //cloned from NSCL/FRIB
  * "MOXA" NPort 5200A series (5250A), NPort IA5000A series (IA5450AI) //TBD. snmp monitoring
  * "MOXA" EDS-G205-1GTXSFP, EDS-G516-4SFP ////TBD. snmp monitoring
  * "MOXA" ioLogik E12xx //TBD. snmp monitoring

99. Analog or digital I/O, no specific EPICS device support
  * "Delta Elektronika BV" DE-SM1500 series SM15-100
  * "Delta Elektronika BV" DE-SM3000 series SM45-70D
  * "BME (Bergmann) PCD High Voltage PSUs (PCD3h, PCD8n7, PCD8g5, PCD8m7)

100. PCI (MRF/Hytec)
  * MRF cPCI-EVG-300, cPCI-EVR-300, cPCI-EVRTG-300
  * MRF cPCI-FCT-8 //TBD. but this is Ethernet(Streams)

101. GigE Vision Cameras (not really in use but tested with areaDetector-1-9-1 & aravisGigE-0-1-15dls6 & aravis-0.2.3)
  * "The Imaging Source" DFK 23GV024
  * "Basler" ACA-1600

999. long shot...
  * PDU Raritan PX2-1730 //snmp monitoring


